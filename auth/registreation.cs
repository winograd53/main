﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BaseClasses;

namespace WindowsFormsApp1.auth
{
    public partial class registreation : BaseForm
    {
        public registreation()
        {
            InitializeComponent();
        }

        private void учетная_записьBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.учетная_записьBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.demDataSet);

        }

        private void registreation_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.роль". При необходимости она может быть перемещена или удалена.
            this.рольTableAdapter.Fill(this.demDataSet.роль);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.сотрудник". При необходимости она может быть перемещена или удалена.
            this.сотрудникTableAdapter.Fill(this.demDataSet.сотрудник);
            this.сотрудникBindingSource.AddNew();
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.учетная_запись". При необходимости она может быть перемещена или удалена.
            this.учетная_записьTableAdapter.Fill(this.demDataSet.учетная_запись);
            this.учетная_записьBindingSource.AddNew();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.сотрудникBindingSource.EndEdit();
            this.сотрудникTableAdapter.Update(this.demDataSet);
            this.рольTableAdapter.Update(this.demDataSet);
            DataRowView t = (DataRowView)this.учетная_записьBindingSource.Current;
            t.Row["сотрудник_номер"] = Convert.ToInt32(((DataRowView)сотрудникBindingSource.Current).Row["номер"]);
            t.Row["роль_номер"] = Convert.ToInt32(((DataRowView)рольBindingSource.Current).Row["номер"]);
            учетная_записьBindingSource.EndEdit();
            this.учетная_записьTableAdapter.Update(this.demDataSet);
            this.Close();
        }
    }
}
