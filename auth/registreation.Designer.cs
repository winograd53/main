﻿namespace WindowsFormsApp1.auth
{
    partial class registreation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label логинLabel;
            System.Windows.Forms.Label парольLabel;
            System.Windows.Forms.Label номерLabel;
            System.Windows.Forms.Label фИОLabel;
            System.Windows.Forms.Label названиеLabel;
            this.demDataSet = new WindowsFormsApp1.demDataSet();
            this.учетная_записьBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.учетная_записьTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.учетная_записьTableAdapter();
            this.tableAdapterManager = new WindowsFormsApp1.demDataSetTableAdapters.TableAdapterManager();
            this.сотрудникTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.сотрудникTableAdapter();
            this.логинTextBox = new System.Windows.Forms.TextBox();
            this.парольTextBox = new System.Windows.Forms.TextBox();
            this.номерTextBox = new System.Windows.Forms.TextBox();
            this.сотрудникBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.фИОTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.рольBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.рольTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.рольTableAdapter();
            this.названиеComboBox = new System.Windows.Forms.ComboBox();
            логинLabel = new System.Windows.Forms.Label();
            парольLabel = new System.Windows.Forms.Label();
            номерLabel = new System.Windows.Forms.Label();
            фИОLabel = new System.Windows.Forms.Label();
            названиеLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.demDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.учетная_записьBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.сотрудникBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.рольBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // логинLabel
            // 
            логинLabel.AutoSize = true;
            логинLabel.Location = new System.Drawing.Point(23, 41);
            логинLabel.Name = "логинLabel";
            логинLabel.Size = new System.Drawing.Size(39, 13);
            логинLabel.TabIndex = 0;
            логинLabel.Text = "логин:";
            // 
            // парольLabel
            // 
            парольLabel.AutoSize = true;
            парольLabel.Location = new System.Drawing.Point(16, 67);
            парольLabel.Name = "парольLabel";
            парольLabel.Size = new System.Drawing.Size(46, 13);
            парольLabel.TabIndex = 2;
            парольLabel.Text = "пароль:";
            // 
            // номерLabel
            // 
            номерLabel.AutoSize = true;
            номерLabel.Location = new System.Drawing.Point(20, 15);
            номерLabel.Name = "номерLabel";
            номерLabel.Size = new System.Drawing.Size(42, 13);
            номерLabel.TabIndex = 4;
            номерLabel.Text = "номер:";
            // 
            // фИОLabel
            // 
            фИОLabel.AutoSize = true;
            фИОLabel.Location = new System.Drawing.Point(25, 103);
            фИОLabel.Name = "фИОLabel";
            фИОLabel.Size = new System.Drawing.Size(37, 13);
            фИОLabel.TabIndex = 6;
            фИОLabel.Text = "ФИО:";
            // 
            // demDataSet
            // 
            this.demDataSet.DataSetName = "demDataSet";
            this.demDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // учетная_записьBindingSource
            // 
            this.учетная_записьBindingSource.DataMember = "учетная_запись";
            this.учетная_записьBindingSource.DataSource = this.demDataSet;
            // 
            // учетная_записьTableAdapter
            // 
            this.учетная_записьTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = WindowsFormsApp1.demDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.заявкаTableAdapter = null;
            this.tableAdapterManager.комментарии_к_работеTableAdapter = null;
            this.tableAdapterManager.комментарийTableAdapter = null;
            this.tableAdapterManager.работаTableAdapter = null;
            this.tableAdapterManager.распределение_ответственныхTableAdapter = null;
            this.tableAdapterManager.рольTableAdapter = null;
            this.tableAdapterManager.сотрудникTableAdapter = this.сотрудникTableAdapter;
            this.tableAdapterManager.список_комментариевTableAdapter = null;
            this.tableAdapterManager.список_работTableAdapter = null;
            this.tableAdapterManager.статус_заявкиTableAdapter = null;
            this.tableAdapterManager.тип_неисправностиTableAdapter = null;
            this.tableAdapterManager.учетная_записьTableAdapter = this.учетная_записьTableAdapter;
            this.tableAdapterManager.этап_выполненияTableAdapter = null;
            // 
            // сотрудникTableAdapter
            // 
            this.сотрудникTableAdapter.ClearBeforeFill = true;
            // 
            // логинTextBox
            // 
            this.логинTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.учетная_записьBindingSource, "логин", true));
            this.логинTextBox.Location = new System.Drawing.Point(68, 38);
            this.логинTextBox.Name = "логинTextBox";
            this.логинTextBox.Size = new System.Drawing.Size(121, 20);
            this.логинTextBox.TabIndex = 1;
            // 
            // парольTextBox
            // 
            this.парольTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.учетная_записьBindingSource, "пароль", true));
            this.парольTextBox.Location = new System.Drawing.Point(68, 64);
            this.парольTextBox.Name = "парольTextBox";
            this.парольTextBox.PasswordChar = '*';
            this.парольTextBox.Size = new System.Drawing.Size(121, 20);
            this.парольTextBox.TabIndex = 3;
            // 
            // номерTextBox
            // 
            this.номерTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.учетная_записьBindingSource, "номер", true));
            this.номерTextBox.Enabled = false;
            this.номерTextBox.Location = new System.Drawing.Point(68, 12);
            this.номерTextBox.Name = "номерTextBox";
            this.номерTextBox.Size = new System.Drawing.Size(121, 20);
            this.номерTextBox.TabIndex = 5;
            // 
            // сотрудникBindingSource
            // 
            this.сотрудникBindingSource.DataMember = "сотрудник";
            this.сотрудникBindingSource.DataSource = this.demDataSet;
            // 
            // фИОTextBox
            // 
            this.фИОTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.сотрудникBindingSource, "ФИО", true));
            this.фИОTextBox.Location = new System.Drawing.Point(68, 100);
            this.фИОTextBox.Name = "фИОTextBox";
            this.фИОTextBox.Size = new System.Drawing.Size(121, 20);
            this.фИОTextBox.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(7, 161);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(182, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "зарегистрироваться";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // рольBindingSource
            // 
            this.рольBindingSource.DataMember = "роль";
            this.рольBindingSource.DataSource = this.demDataSet;
            // 
            // рольTableAdapter
            // 
            this.рольTableAdapter.ClearBeforeFill = true;
            // 
            // названиеLabel
            // 
            названиеLabel.AutoSize = true;
            названиеLabel.Location = new System.Drawing.Point(28, 137);
            названиеLabel.Name = "названиеLabel";
            названиеLabel.Size = new System.Drawing.Size(34, 13);
            названиеLabel.TabIndex = 9;
            названиеLabel.Text = "роль:";
            // 
            // названиеComboBox
            // 
            this.названиеComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.рольBindingSource, "название", true));
            this.названиеComboBox.DataSource = this.рольBindingSource;
            this.названиеComboBox.DisplayMember = "название";
            this.названиеComboBox.FormattingEnabled = true;
            this.названиеComboBox.Location = new System.Drawing.Point(68, 134);
            this.названиеComboBox.Name = "названиеComboBox";
            this.названиеComboBox.Size = new System.Drawing.Size(121, 21);
            this.названиеComboBox.TabIndex = 10;
            this.названиеComboBox.ValueMember = "название";
            // 
            // registreation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(211, 204);
            this.Controls.Add(названиеLabel);
            this.Controls.Add(this.названиеComboBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(фИОLabel);
            this.Controls.Add(this.фИОTextBox);
            this.Controls.Add(номерLabel);
            this.Controls.Add(this.номерTextBox);
            this.Controls.Add(парольLabel);
            this.Controls.Add(this.парольTextBox);
            this.Controls.Add(логинLabel);
            this.Controls.Add(this.логинTextBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "registreation";
            this.Text = "регистрация";
            this.Load += new System.EventHandler(this.registreation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.demDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.учетная_записьBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.сотрудникBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.рольBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private demDataSet demDataSet;
        private System.Windows.Forms.BindingSource учетная_записьBindingSource;
        private demDataSetTableAdapters.учетная_записьTableAdapter учетная_записьTableAdapter;
        private demDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource сотрудникBindingSource;
        private demDataSetTableAdapters.сотрудникTableAdapter сотрудникTableAdapter;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox логинTextBox;
        public System.Windows.Forms.TextBox парольTextBox;
        public System.Windows.Forms.TextBox номерTextBox;
        public System.Windows.Forms.TextBox фИОTextBox;
        private System.Windows.Forms.BindingSource рольBindingSource;
        private demDataSetTableAdapters.рольTableAdapter рольTableAdapter;
        public System.Windows.Forms.ComboBox названиеComboBox;
    }
}