﻿namespace WindowsFormsApp1.auth
{
    partial class auth
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.demDataSet = new WindowsFormsApp1.demDataSet();
            this.учетная_записьBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.учетная_записьTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.учетная_записьTableAdapter();
            this.tableAdapterManager = new WindowsFormsApp1.demDataSetTableAdapters.TableAdapterManager();
            this.рольTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.рольTableAdapter();
            this.рольBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.demDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.учетная_записьBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.рольBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(75, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 0;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(75, 38);
            this.textBox2.Name = "textBox2";
            this.textBox2.PasswordChar = '*';
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "логин";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "пароль";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 71);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "войти";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // demDataSet
            // 
            this.demDataSet.DataSetName = "demDataSet";
            this.demDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // учетная_записьBindingSource
            // 
            this.учетная_записьBindingSource.DataMember = "учетная_запись";
            this.учетная_записьBindingSource.DataSource = this.demDataSet;
            // 
            // учетная_записьTableAdapter
            // 
            this.учетная_записьTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = WindowsFormsApp1.demDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.заявкаTableAdapter = null;
            this.tableAdapterManager.комментарии_к_работеTableAdapter = null;
            this.tableAdapterManager.комментарийTableAdapter = null;
            this.tableAdapterManager.работаTableAdapter = null;
            this.tableAdapterManager.распределение_ответственныхTableAdapter = null;
            this.tableAdapterManager.рольTableAdapter = this.рольTableAdapter;
            this.tableAdapterManager.сотрудникTableAdapter = null;
            this.tableAdapterManager.список_комментариевTableAdapter = null;
            this.tableAdapterManager.список_работTableAdapter = null;
            this.tableAdapterManager.статус_заявкиTableAdapter = null;
            this.tableAdapterManager.тип_неисправностиTableAdapter = null;
            this.tableAdapterManager.учетная_записьTableAdapter = this.учетная_записьTableAdapter;
            this.tableAdapterManager.этап_выполненияTableAdapter = null;
            // 
            // рольTableAdapter
            // 
            this.рольTableAdapter.ClearBeforeFill = true;
            // 
            // рольBindingSource
            // 
            this.рольBindingSource.DataMember = "роль";
            this.рольBindingSource.DataSource = this.demDataSet;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(15, 100);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(160, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "регистрация";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // auth
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(188, 138);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "auth";
            this.Text = "Авторизация";
            this.Load += new System.EventHandler(this.auth_Load);
            ((System.ComponentModel.ISupportInitialize)(this.demDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.учетная_записьBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.рольBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private demDataSet demDataSet;
        private System.Windows.Forms.BindingSource учетная_записьBindingSource;
        private demDataSetTableAdapters.учетная_записьTableAdapter учетная_записьTableAdapter;
        private demDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource рольBindingSource;
        private demDataSetTableAdapters.рольTableAdapter рольTableAdapter;
        private System.Windows.Forms.Button button2;
    }
}