﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BaseClasses;

namespace WindowsFormsApp1.auth
{
    public partial class auth : BaseForm
    {
        public int id;
        public string login;
        public string password;
        public string role;
        public auth()
        {
            InitializeComponent();
        }

        private void учетная_записьBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.учетная_записьBindingSource.EndEdit();

        }

        private void auth_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.роль". При необходимости она может быть перемещена или удалена.
            this.рольTableAdapter.Fill(this.demDataSet.роль);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.учетная_запись". При необходимости она может быть перемещена или удалена.
            this.учетная_записьTableAdapter.Fill(this.demDataSet.учетная_запись);

        }

        private void button1_Click(object sender, EventArgs e)
        {

            string filter = $"логин = '{this.textBox1.Text}' AND пароль = '{textBox2.Text}'";
            this.учетная_записьBindingSource.Filter = filter;
            if(учетная_записьBindingSource.Count > 0 && учетная_записьBindingSource.Count < 2)
            {
                this.login = textBox1.Text;
                this.password = textBox2.Text;
                DataRowView row = учетная_записьBindingSource.Current as DataRowView;
                if(row != null)
                {
                    this.id = Convert.ToInt32(row.Row["номер"]);
                    int roleId = Convert.ToInt32(row.Row["роль_номер"]);
                    this.рольTableAdapter.Fill(this.demDataSet.роль);
                    string newFilter = $"номер='{roleId}'";
                    this.рольBindingSource.Filter = newFilter;
                    DataRowView currentRoleRow = рольBindingSource.Current as DataRowView;
                    if(currentRoleRow != null)
                    {
                        this.role = Convert.ToString(currentRoleRow.Row["название"]);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Вы авторизовались, но для вашей учетной записи не найдена роль");
                    }
                }
            }
            else
            {
                MessageBox.Show("Неверный логин или пароль");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            registreation registreation = new registreation();
            registreation.ShowDialog();
            this.id = int.Parse(registreation.номерTextBox.Text);
            if(registreation.логинTextBox.Text == "" || registreation.парольTextBox.Text == "")
            {
                MessageBox.Show("Вы не заполнили пароль или логин при регистрации");
                return;
            }
            this.login = registreation.логинTextBox.Text;
            this.password = registreation.парольTextBox.Text;
            DataRowView r = (DataRowView)registreation.названиеComboBox.SelectedItem;
            this.role = r.Row["название"].ToString();
            this.textBox1.Text = this.login;
            this.textBox2.Text = this.password;
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.роль". При необходимости она может быть перемещена или удалена.
            this.рольTableAdapter.Fill(this.demDataSet.роль);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.учетная_запись". При необходимости она может быть перемещена или удалена.
            this.учетная_записьTableAdapter.Fill(this.demDataSet.учетная_запись);
            this.button1_Click(null, null);
        }
    }
}
