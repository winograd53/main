﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.auth;
using WindowsFormsApp1.request;

namespace WindowsFormsApp1
{
    internal static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            auth.auth auth = new auth.auth();
            Application.Run(auth);
            if (auth.id != 0)
            {
                if(auth.role == "администратор")
                {
                    Application.Run(new add());
                }
            }
        }
    }
}
