﻿namespace WindowsFormsApp1.request
{
    partial class WorkingList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorkingList));
            this.demDataSet = new WindowsFormsApp1.demDataSet();
            this.список_работBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.список_работTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.список_работTableAdapter();
            this.tableAdapterManager = new WindowsFormsApp1.demDataSetTableAdapters.TableAdapterManager();
            this.список_работBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.список_работBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.список_работDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.заявкаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.сотрудникBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.заявкаTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.заявкаTableAdapter();
            this.сотрудникTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.сотрудникTableAdapter();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.описаниеTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.demDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.список_работBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.список_работBindingNavigator)).BeginInit();
            this.список_работBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.список_работDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.заявкаBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.сотрудникBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // demDataSet
            // 
            this.demDataSet.DataSetName = "demDataSet";
            this.demDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // список_работBindingSource
            // 
            this.список_работBindingSource.DataMember = "список_работ";
            this.список_работBindingSource.DataSource = this.demDataSet;
            // 
            // список_работTableAdapter
            // 
            this.список_работTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = WindowsFormsApp1.demDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.заявкаTableAdapter = null;
            this.tableAdapterManager.клиентTableAdapter = null;
            this.tableAdapterManager.комментарии_к_работеTableAdapter = null;
            this.tableAdapterManager.комментарийTableAdapter = null;
            this.tableAdapterManager.работаTableAdapter = null;
            this.tableAdapterManager.распределение_ответственныхTableAdapter = null;
            this.tableAdapterManager.рольTableAdapter = null;
            this.tableAdapterManager.сотрудникTableAdapter = null;
            this.tableAdapterManager.список_комментариевTableAdapter = null;
            this.tableAdapterManager.список_работTableAdapter = this.список_работTableAdapter;
            this.tableAdapterManager.статус_заявкиTableAdapter = null;
            this.tableAdapterManager.тип_неисправностиTableAdapter = null;
            this.tableAdapterManager.учетная_записьTableAdapter = null;
            this.tableAdapterManager.этап_выполненияTableAdapter = null;
            // 
            // список_работBindingNavigator
            // 
            this.список_работBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.список_работBindingNavigator.BindingSource = this.список_работBindingSource;
            this.список_работBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.список_работBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.список_работBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.список_работBindingNavigatorSaveItem});
            this.список_работBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.список_работBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.список_работBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.список_работBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.список_работBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.список_работBindingNavigator.Name = "список_работBindingNavigator";
            this.список_работBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.список_работBindingNavigator.Size = new System.Drawing.Size(800, 25);
            this.список_работBindingNavigator.TabIndex = 0;
            this.список_работBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(43, 22);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // список_работBindingNavigatorSaveItem
            // 
            this.список_работBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.список_работBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("список_работBindingNavigatorSaveItem.Image")));
            this.список_работBindingNavigatorSaveItem.Name = "список_работBindingNavigatorSaveItem";
            this.список_работBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.список_работBindingNavigatorSaveItem.Text = "Сохранить данные";
            this.список_работBindingNavigatorSaveItem.Click += new System.EventHandler(this.список_работBindingNavigatorSaveItem_Click);
            // 
            // список_работDataGridView
            // 
            this.список_работDataGridView.AutoGenerateColumns = false;
            this.список_работDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.список_работDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.список_работDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.список_работDataGridView.DataSource = this.список_работBindingSource;
            this.список_работDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.список_работDataGridView.Location = new System.Drawing.Point(0, 25);
            this.список_работDataGridView.Name = "список_работDataGridView";
            this.список_работDataGridView.Size = new System.Drawing.Size(800, 408);
            this.список_работDataGridView.TabIndex = 1;
            this.список_работDataGridView.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.список_работDataGridView_DefaultValuesNeeded);
            this.список_работDataGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.список_работDataGridView_RowsAdded);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "номер";
            this.dataGridViewTextBoxColumn1.HeaderText = "номер";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "заявка_номер";
            this.dataGridViewTextBoxColumn2.DataSource = this.заявкаBindingSource;
            this.dataGridViewTextBoxColumn2.DisplayMember = "номер";
            this.dataGridViewTextBoxColumn2.HeaderText = "заявка_номер";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn2.ValueMember = "номер";
            // 
            // заявкаBindingSource
            // 
            this.заявкаBindingSource.DataMember = "заявка";
            this.заявкаBindingSource.DataSource = this.demDataSet;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "сотрудник_номер";
            this.dataGridViewTextBoxColumn3.DataSource = this.сотрудникBindingSource;
            this.dataGridViewTextBoxColumn3.DisplayMember = "ФИО";
            this.dataGridViewTextBoxColumn3.HeaderText = "сотрудник";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn3.ValueMember = "номер";
            // 
            // сотрудникBindingSource
            // 
            this.сотрудникBindingSource.DataMember = "сотрудник";
            this.сотрудникBindingSource.DataSource = this.demDataSet;
            // 
            // заявкаTableAdapter
            // 
            this.заявкаTableAdapter.ClearBeforeFill = true;
            // 
            // сотрудникTableAdapter
            // 
            this.сотрудникTableAdapter.ClearBeforeFill = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.описаниеTextBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 219);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(800, 214);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Описание проблемы:";
            // 
            // описаниеTextBox
            // 
            this.описаниеTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.список_работBindingSource, "описание", true));
            this.описаниеTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.описаниеTextBox.Location = new System.Drawing.Point(3, 16);
            this.описаниеTextBox.Multiline = true;
            this.описаниеTextBox.Name = "описаниеTextBox";
            this.описаниеTextBox.Size = new System.Drawing.Size(794, 195);
            this.описаниеTextBox.TabIndex = 1;
            // 
            // WorkingList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 433);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.список_работDataGridView);
            this.Controls.Add(this.список_работBindingNavigator);
            this.Name = "WorkingList";
            this.Text = "workingList";
            this.Load += new System.EventHandler(this.WorkingList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.demDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.список_работBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.список_работBindingNavigator)).EndInit();
            this.список_работBindingNavigator.ResumeLayout(false);
            this.список_работBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.список_работDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.заявкаBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.сотрудникBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private demDataSet demDataSet;
        private System.Windows.Forms.BindingSource список_работBindingSource;
        private demDataSetTableAdapters.список_работTableAdapter список_работTableAdapter;
        private demDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator список_работBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton список_работBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView список_работDataGridView;
        private System.Windows.Forms.BindingSource заявкаBindingSource;
        private demDataSetTableAdapters.заявкаTableAdapter заявкаTableAdapter;
        private System.Windows.Forms.BindingSource сотрудникBindingSource;
        private demDataSetTableAdapters.сотрудникTableAdapter сотрудникTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn3;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox описаниеTextBox;
    }
}