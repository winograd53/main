﻿namespace WindowsFormsApp1.request
{
    partial class add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(add));
            this.demDataSet = new WindowsFormsApp1.demDataSet();
            this.заявкаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.заявкаTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.заявкаTableAdapter();
            this.tableAdapterManager = new WindowsFormsApp1.demDataSetTableAdapters.TableAdapterManager();
            this.заявкаBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.заявкаBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.заявкаDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.тип_неисправностиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.клиентBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.статус_заявкиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.этап_выполненияBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.тип_неисправностиTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.тип_неисправностиTableAdapter();
            this.клиентTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.клиентTableAdapter();
            this.статус_заявкиTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.статус_заявкиTableAdapter();
            this.этап_выполненияTableAdapter = new WindowsFormsApp1.demDataSetTableAdapters.этап_выполненияTableAdapter();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.открытьСписокРаботToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.demDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.заявкаBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.заявкаBindingNavigator)).BeginInit();
            this.заявкаBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.заявкаDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.тип_неисправностиBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.клиентBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.статус_заявкиBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.этап_выполненияBindingSource)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // demDataSet
            // 
            this.demDataSet.DataSetName = "demDataSet";
            this.demDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // заявкаBindingSource
            // 
            this.заявкаBindingSource.DataMember = "заявка";
            this.заявкаBindingSource.DataSource = this.demDataSet;
            // 
            // заявкаTableAdapter
            // 
            this.заявкаTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = WindowsFormsApp1.demDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.заявкаTableAdapter = this.заявкаTableAdapter;
            this.tableAdapterManager.клиентTableAdapter = null;
            this.tableAdapterManager.комментарии_к_работеTableAdapter = null;
            this.tableAdapterManager.комментарийTableAdapter = null;
            this.tableAdapterManager.работаTableAdapter = null;
            this.tableAdapterManager.распределение_ответственныхTableAdapter = null;
            this.tableAdapterManager.рольTableAdapter = null;
            this.tableAdapterManager.сотрудникTableAdapter = null;
            this.tableAdapterManager.список_комментариевTableAdapter = null;
            this.tableAdapterManager.список_работTableAdapter = null;
            this.tableAdapterManager.статус_заявкиTableAdapter = null;
            this.tableAdapterManager.тип_неисправностиTableAdapter = null;
            this.tableAdapterManager.учетная_записьTableAdapter = null;
            this.tableAdapterManager.этап_выполненияTableAdapter = null;
            // 
            // заявкаBindingNavigator
            // 
            this.заявкаBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.заявкаBindingNavigator.BindingSource = this.заявкаBindingSource;
            this.заявкаBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.заявкаBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.заявкаBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.заявкаBindingNavigatorSaveItem});
            this.заявкаBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.заявкаBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.заявкаBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.заявкаBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.заявкаBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.заявкаBindingNavigator.Name = "заявкаBindingNavigator";
            this.заявкаBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.заявкаBindingNavigator.Size = new System.Drawing.Size(844, 25);
            this.заявкаBindingNavigator.TabIndex = 0;
            this.заявкаBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(43, 22);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // заявкаBindingNavigatorSaveItem
            // 
            this.заявкаBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.заявкаBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("заявкаBindingNavigatorSaveItem.Image")));
            this.заявкаBindingNavigatorSaveItem.Name = "заявкаBindingNavigatorSaveItem";
            this.заявкаBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.заявкаBindingNavigatorSaveItem.Text = "Сохранить данные";
            this.заявкаBindingNavigatorSaveItem.Click += new System.EventHandler(this.заявкаBindingNavigatorSaveItem_Click);
            // 
            // заявкаDataGridView
            // 
            this.заявкаDataGridView.AutoGenerateColumns = false;
            this.заявкаDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.заявкаDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.заявкаDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.заявкаDataGridView.ContextMenuStrip = this.contextMenuStrip1;
            this.заявкаDataGridView.DataSource = this.заявкаBindingSource;
            this.заявкаDataGridView.Dock = System.Windows.Forms.DockStyle.Left;
            this.заявкаDataGridView.Location = new System.Drawing.Point(0, 25);
            this.заявкаDataGridView.Name = "заявкаDataGridView";
            this.заявкаDataGridView.Size = new System.Drawing.Size(844, 438);
            this.заявкаDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "номер";
            this.dataGridViewTextBoxColumn1.HeaderText = "номер";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "дата_добавления";
            this.dataGridViewTextBoxColumn2.HeaderText = "дата_добавления";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "тип_неисправности";
            this.dataGridViewTextBoxColumn3.DataSource = this.тип_неисправностиBindingSource;
            this.dataGridViewTextBoxColumn3.DisplayMember = "название";
            this.dataGridViewTextBoxColumn3.HeaderText = "тип_неисправности";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn3.ValueMember = "номер";
            // 
            // тип_неисправностиBindingSource
            // 
            this.тип_неисправностиBindingSource.DataMember = "тип_неисправности";
            this.тип_неисправностиBindingSource.DataSource = this.demDataSet;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "описание_проблемы";
            this.dataGridViewTextBoxColumn4.HeaderText = "описание_проблемы";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "клиент";
            this.dataGridViewTextBoxColumn5.DataSource = this.клиентBindingSource;
            this.dataGridViewTextBoxColumn5.DisplayMember = "фио";
            this.dataGridViewTextBoxColumn5.HeaderText = "клиент";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn5.ValueMember = "номер";
            // 
            // клиентBindingSource
            // 
            this.клиентBindingSource.DataMember = "клиент";
            this.клиентBindingSource.DataSource = this.demDataSet;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "статус";
            this.dataGridViewTextBoxColumn6.DataSource = this.статус_заявкиBindingSource;
            this.dataGridViewTextBoxColumn6.DisplayMember = "название";
            this.dataGridViewTextBoxColumn6.HeaderText = "статус";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn6.ValueMember = "номер";
            // 
            // статус_заявкиBindingSource
            // 
            this.статус_заявкиBindingSource.DataMember = "статус_заявки";
            this.статус_заявкиBindingSource.DataSource = this.demDataSet;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "этап_выполнения";
            this.dataGridViewTextBoxColumn7.DataSource = this.этап_выполненияBindingSource;
            this.dataGridViewTextBoxColumn7.DisplayMember = "название";
            this.dataGridViewTextBoxColumn7.HeaderText = "этап_выполнения";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn7.ValueMember = "номер";
            // 
            // этап_выполненияBindingSource
            // 
            this.этап_выполненияBindingSource.DataMember = "этап_выполнения";
            this.этап_выполненияBindingSource.DataSource = this.demDataSet;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "оборудование_название";
            this.dataGridViewTextBoxColumn8.HeaderText = "оборудование_название";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // тип_неисправностиTableAdapter
            // 
            this.тип_неисправностиTableAdapter.ClearBeforeFill = true;
            // 
            // клиентTableAdapter
            // 
            this.клиентTableAdapter.ClearBeforeFill = true;
            // 
            // статус_заявкиTableAdapter
            // 
            this.статус_заявкиTableAdapter.ClearBeforeFill = true;
            // 
            // этап_выполненияTableAdapter
            // 
            this.этап_выполненияTableAdapter.ClearBeforeFill = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьСписокРаботToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(197, 48);
            // 
            // открытьСписокРаботToolStripMenuItem
            // 
            this.открытьСписокРаботToolStripMenuItem.Name = "открытьСписокРаботToolStripMenuItem";
            this.открытьСписокРаботToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.открытьСписокРаботToolStripMenuItem.Text = "открыть список работ";
            this.открытьСписокРаботToolStripMenuItem.Click += new System.EventHandler(this.открытьСписокРаботToolStripMenuItem_Click);
            // 
            // add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 463);
            this.Controls.Add(this.заявкаDataGridView);
            this.Controls.Add(this.заявкаBindingNavigator);
            this.Name = "add";
            this.Text = "Заявки";
            this.Load += new System.EventHandler(this.add_Load);
            ((System.ComponentModel.ISupportInitialize)(this.demDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.заявкаBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.заявкаBindingNavigator)).EndInit();
            this.заявкаBindingNavigator.ResumeLayout(false);
            this.заявкаBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.заявкаDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.тип_неисправностиBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.клиентBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.статус_заявкиBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.этап_выполненияBindingSource)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private demDataSet demDataSet;
        private System.Windows.Forms.BindingSource заявкаBindingSource;
        private demDataSetTableAdapters.заявкаTableAdapter заявкаTableAdapter;
        private demDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator заявкаBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton заявкаBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView заявкаDataGridView;
        private System.Windows.Forms.BindingSource тип_неисправностиBindingSource;
        private demDataSetTableAdapters.тип_неисправностиTableAdapter тип_неисправностиTableAdapter;
        private System.Windows.Forms.BindingSource клиентBindingSource;
        private demDataSetTableAdapters.клиентTableAdapter клиентTableAdapter;
        private System.Windows.Forms.BindingSource статус_заявкиBindingSource;
        private demDataSetTableAdapters.статус_заявкиTableAdapter статус_заявкиTableAdapter;
        private System.Windows.Forms.BindingSource этап_выполненияBindingSource;
        private demDataSetTableAdapters.этап_выполненияTableAdapter этап_выполненияTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem открытьСписокРаботToolStripMenuItem;
    }
}