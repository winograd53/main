﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BaseClasses;

namespace WindowsFormsApp1.request
{
    public partial class add : BaseForm
    {
        public add()
        {
            InitializeComponent();
        }

        private void заявкаBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.заявкаBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.demDataSet);

        }

        private void add_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.этап_выполнения". При необходимости она может быть перемещена или удалена.
            this.этап_выполненияTableAdapter.Fill(this.demDataSet.этап_выполнения);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.статус_заявки". При необходимости она может быть перемещена или удалена.
            this.статус_заявкиTableAdapter.Fill(this.demDataSet.статус_заявки);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.клиент". При необходимости она может быть перемещена или удалена.
            this.клиентTableAdapter.Fill(this.demDataSet.клиент);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.тип_неисправности". При необходимости она может быть перемещена или удалена.
            this.тип_неисправностиTableAdapter.Fill(this.demDataSet.тип_неисправности);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.тип_неисправности". При необходимости она может быть перемещена или удалена.
            this.тип_неисправностиTableAdapter.Fill(this.demDataSet.тип_неисправности);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.заявка". При необходимости она может быть перемещена или удалена.
            this.заявкаTableAdapter.Fill(this.demDataSet.заявка);

        }

        private void открытьСписокРаботToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataGridViewRow dataGridViewRow = заявкаDataGridView.SelectedRows[0];
            int id = Convert.ToInt32(dataGridViewRow.Cells[0].Value);
            WorkingList workingList = new WorkingList(id);
            workingList.Show(this);
        }
    }
}
