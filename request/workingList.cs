﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.BaseClasses;

namespace WindowsFormsApp1.request
{
    public partial class WorkingList : BaseForm
    {
        int id;
        public WorkingList(int id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void список_работBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.список_работBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.demDataSet);

        }

        private void WorkingList_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.сотрудник". При необходимости она может быть перемещена или удалена.
            this.сотрудникTableAdapter.Fill(this.demDataSet.сотрудник);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.заявка". При необходимости она может быть перемещена или удалена.
            this.заявкаTableAdapter.Fill(this.demDataSet.заявка);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "demDataSet.список_работ". При необходимости она может быть перемещена или удалена.
            this.список_работTableAdapter.Fill(this.demDataSet.список_работ);
            this.список_работBindingSource.Filter = $"заявка_номер='{this.id}'";
        }

        private void список_работDataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

        }

        private void список_работDataGridView_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            // Проверяем, что добавлена хотя бы одна строка
            if (e.Row.Index >= 0)
            {
                // Получаем индекс последней добавленной строки
                int rowIndex = e.Row.Index;
                список_работDataGridView.Rows[rowIndex].Cells["dataGridViewTextBoxColumn2"].Value = this.id;
            }
        }
    }
}
